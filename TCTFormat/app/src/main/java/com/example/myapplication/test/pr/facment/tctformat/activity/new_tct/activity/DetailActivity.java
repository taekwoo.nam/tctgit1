package com.example.myapplication.test.pr.facment.tctformat.activity.new_tct.activity;

import android.os.Bundle;

import androidx.appcompat.app.AppCompatActivity;
import androidx.room.Room;

import com.example.myapplication.test.pr.facment.tctformat.R;

import java.util.List;

public class DetailActivity extends AppCompatActivity {

    private NoteDatabase noteDatabase;
    @Override
    public void onCreate(Bundle bundle) {
        super.onCreate(bundle);
        setContentView(R.layout.detail_activity);

        // Room 데이터베이스 인스턴스 생성
        noteDatabase = Room.databaseBuilder(getApplicationContext(),
                NoteDatabase.class, "note_database").build();

        // 데이터베이스 작업 실행 (예: 데이터 삽입 및 조회)
        insertNote();
        List<Note> notes = noteDatabase.noteDao().getAllNotes();




    }

    private void insertNote() {
        // 데이터 삽입 예시
        new Thread(() -> {
            Note note = new Note("Title", "Content");
            noteDatabase.noteDao().insert(note);
        }).start();
    }
}
