package com.example.myapplication.test.pr.facment.tctformat.contract;

import android.util.Log;

import com.example.myapplication.test.pr.facment.tctformat.data.response.Data1;
import com.example.myapplication.test.pr.facment.tctformat.data.response.ExpKey;
import com.example.myapplication.test.pr.facment.tctformat.data.response.ExpSubData;
import com.example.myapplication.test.pr.facment.tctformat.network.retrofit.RetrofitServiceFactory;

import java.io.IOException;
import java.nio.charset.StandardCharsets;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class ExListPresenter implements ExListContract.Presenter<ExListContract.View> {
    private ExListContract.View view;

    @Override
    public void setView(ExListContract.View view) {
        this.view = view;
    }

    public void requestData() {

        view.setDatas(new ArrayList<ExpKey>(getData().keySet()), getData());


//        RetrofitServiceFactory.dataGetService().requestData1().enqueue(new Callback<Data1>() {
//            @Override
//            public void onResponse(Call<Data1> call, Response<Data1> response) {
//
//                Log.e("taekwoo.nam", "ExListPresenter::requestData::onResponse ");
//
//                view.setDatas(new ArrayList<ExpKey>(getData().keySet()), getData());
////                view.setDatas(response.body());
//
//                //JSON String 데이터 파싱
////                byte[] bytes = "".getBytes(StandardCharsets.UTF_8);
////
////                try {
////                    bytes = response.body().bytes();
////                } catch (IOException e) {
////                    e.printStackTrace();
////                }
////                String token = new String(bytes, StandardCharsets.UTF_8);
//            }
//
//            @Override
//            public void onFailure(Call<Data1> call, Throwable t) {
//                Log.e("taekwoo.nam", "ExListPresenter::requestData::onFailure : "  + t.getMessage());
//            }
//        });
    }

    public HashMap<ExpKey, List<ExpSubData>> getData() {
        HashMap<ExpKey, List<ExpSubData>> expandableListDetail = new HashMap<ExpKey, List<ExpSubData>>();
        ArrayList<ExpKey> expKeyArrayList = new ArrayList<>();
        expKeyArrayList.add(new ExpKey("key1", 1, "image_file1.png"));
        expKeyArrayList.add(new ExpKey("key2", 2, "image_file2.png"));
        expKeyArrayList.add(new ExpKey("key3", 3, "image_file3.png"));
        expKeyArrayList.add(new ExpKey("key4", 4, "image_file4.png"));
        expKeyArrayList.add(new ExpKey("key5", 5, "image_file5.png"));

        ArrayList<List<ExpSubData>> inputSubData = new ArrayList<>();

        ArrayList<ExpSubData> expSubData1 = new ArrayList<>();
        expSubData1.add(new ExpSubData("data1", "data2", "data3"));
        expSubData1.add(new ExpSubData("data11", "data22", "data33"));
        expSubData1.add(new ExpSubData("data111", "data222", "data333"));
        expSubData1.add(new ExpSubData("data1111", "data2222", "data3333"));
        expSubData1.add(new ExpSubData("data11111", "data22222", "data33333"));
        expSubData1.add(new ExpSubData("data111111", "data222222", "data333333"));

        ArrayList<ExpSubData> expSubData2 = new ArrayList<>();
        expSubData2.add(new ExpSubData("data1", "data2", "data3"));


        ArrayList<ExpSubData> expSubData3 = new ArrayList<>();
        expSubData3.add(new ExpSubData("data1", "data2", "data3"));
        expSubData3.add(new ExpSubData("data11", "data22", "data33"));


        ArrayList<ExpSubData> expSubData4 = new ArrayList<>();
        expSubData4.add(new ExpSubData("data1", "data2", "data3"));
        expSubData4.add(new ExpSubData("data11", "data22", "data33"));
        expSubData4.add(new ExpSubData("data111", "data222", "data333"));

        ArrayList<ExpSubData> expSubData5 = new ArrayList<>();
        expSubData5.add(new ExpSubData("data1", "data2", "data3"));
        expSubData5.add(new ExpSubData("data11", "data22", "data33"));
        expSubData5.add(new ExpSubData("data111", "data222", "data333"));
        expSubData5.add(new ExpSubData("data1111", "data2222", "data3333"));


        inputSubData.add(expSubData1);
        inputSubData.add(expSubData2);
        inputSubData.add(expSubData3);
        inputSubData.add(expSubData4);
        inputSubData.add(expSubData5);

        for (int i = 0; i < expKeyArrayList.size(); i++) {
            expandableListDetail.put(expKeyArrayList.get(i), inputSubData.get(i));
        }

        return expandableListDetail;
    }


//    public HashMap<String, List<String>> getData() {
//        HashMap<String, List<String>> expandableListDetail = new HashMap<String, List<String>>();
//
//        List<String> cricket = new ArrayList<String>();
//        cricket.add("India");
//        cricket.add("Pakistan");
//        cricket.add("Australia");
//        cricket.add("England");
//        cricket.add("South Africa");
//
//        List<String> football = new ArrayList<String>();
//        football.add("Brazil");
//        football.add("Spain");
//        football.add("Germany");
//        football.add("Netherlands");
//        football.add("Italy");
//
//        List<String> basketball = new ArrayList<String>();
//        basketball.add("United States");
//        basketball.add("Spain");
//        basketball.add("Argentina");
//        basketball.add("France");
//        basketball.add("Russia");
//
//        expandableListDetail.put("CRICKET TEAMS", cricket);
//        expandableListDetail.put("FOOTBALL TEAMS", football);
//        expandableListDetail.put("BASKETBALL TEAMS", basketball);
//        return expandableListDetail;
//    }
}
