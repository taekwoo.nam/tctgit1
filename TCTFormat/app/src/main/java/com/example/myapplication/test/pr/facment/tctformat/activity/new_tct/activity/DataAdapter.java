package com.example.myapplication.test.pr.facment.tctformat.activity.new_tct.activity;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.example.myapplication.test.pr.facment.tctformat.R;

import java.util.ArrayList;

public class DataAdapter extends RecyclerView.Adapter<DataAdapter.DataViewHolder>{
    private ArrayList<DataItem> dataList;
    private OnItemClickListener clickListener;

    public DataAdapter(ArrayList<DataItem> list, OnItemClickListener onClick) {
        this.dataList = list;
        this.clickListener = onClick;
    }

    @NonNull
    @Override
    public DataViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.item_layout, parent, false);
        return new DataViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull DataViewHolder holder, int position) {
        holder.itemText.setText(dataList.get(position).getData1());
        holder.itemText1.setText(dataList.get(position).getData2());
        holder.itemText2.setText(dataList.get(position).getData3());
    }

    @Override
    public int getItemCount() {
        return dataList.size();
    }

    interface OnItemClickListener {
        void onItemClick(int position);
    }

    public class DataViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {
        TextView itemText;
        TextView itemText1;
        TextView itemText2;

        public DataViewHolder(View itemView) {
            super(itemView);
            itemText = itemView.findViewById(R.id.itemText);
            itemText = itemView.findViewById(R.id.itemText1);
            itemText = itemView.findViewById(R.id.itemText2);
            itemView.setOnClickListener(this);
        }

        @Override
        public void onClick(View v) {
            if (clickListener != null) {
                clickListener.onItemClick(getAdapterPosition());
            }
        }
    }

}
