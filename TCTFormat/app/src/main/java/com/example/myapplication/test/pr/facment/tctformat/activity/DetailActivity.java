package com.example.myapplication.test.pr.facment.tctformat.activity;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;

import androidx.appcompat.app.AppCompatActivity;
import androidx.databinding.DataBindingUtil;

import com.example.myapplication.test.pr.facment.tctformat.R;
import com.example.myapplication.test.pr.facment.tctformat.contract.DetailContract;
import com.example.myapplication.test.pr.facment.tctformat.contract.DetailPresenter;
import com.example.myapplication.test.pr.facment.tctformat.contract.MainContract;
import com.example.myapplication.test.pr.facment.tctformat.contract.MainPresenter;
import com.example.myapplication.test.pr.facment.tctformat.databinding.ActivityMainBinding;
import com.example.myapplication.test.pr.facment.tctformat.databinding.DetailActivityBinding;

public class DetailActivity extends AppCompatActivity implements DetailContract.View{
    private DetailActivityBinding binding;
    private DetailContract.Presenter presenter;


    public static void starter(Context context,Bundle bundle) {
        Intent intent = new Intent(context, DetailActivity.class);
        intent.putExtras(bundle);
        context.startActivity(intent);
    }

    public static void starter(Context context) {
        Intent intent = new Intent(context, DetailActivity.class);
        context.startActivity(intent);
    }


    @Override
    public void onCreate(Bundle bundle) {
        super.onCreate(bundle);
        binding = DataBindingUtil.setContentView(this, R.layout.detail_activity);
        presenter = new DetailPresenter();
        presenter.setView(this);
        binding.setPresenter(presenter);
    }
}
