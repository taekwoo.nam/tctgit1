package com.example.myapplication.test.pr.facment.tctformat.contract;

import android.util.Log;

import com.example.myapplication.test.pr.facment.tctformat.data.response.Data1;
import com.example.myapplication.test.pr.facment.tctformat.network.retrofit.RetrofitServiceFactory;

import java.util.ArrayList;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class MainPresenter implements MainContract.Presenter<MainContract.View> {
    private MainContract.View view;

    @Override
    public void setView(MainContract.View view) {
        this.view = view;
    }

    @Override
    public void requestDates() {
        Log.e("taekwoo.nam", "requestDates");
        requestData();
    }

    public void requestData() {
        RetrofitServiceFactory.dataGetService().requestData1().enqueue(new Callback<Data1>() {
            @Override
            public void onResponse(Call<Data1> call, Response<Data1> response) {
                Data1 data = response.body();
                Log.e("taekwoo.nam", "onResponse::");

            }

            @Override
            public void onFailure(Call<Data1> call, Throwable t) {
                Log.e("taekwoo.nam", "onFailure::"+t.getMessage());
                ArrayList<Data1> data1ArrayList = new ArrayList<>();
                data1ArrayList.add(new Data1("value"));
                data1ArrayList.add(new Data1("adsfawefue"));
                data1ArrayList.add(new Data1("sdef"));
                data1ArrayList.add(new Data1("134hjfkm"));
                data1ArrayList.add(new Data1("lfgbsrtaer"));
                data1ArrayList.add(new Data1("rvbnsrvalvaue"));
                data1ArrayList.add(new Data1("kmaregvalue"));
                data1ArrayList.add(new Data1("djkfhkvalue1"));
                data1ArrayList.add(new Data1("ekfjhvalue2"));
                view.setDatas(data1ArrayList);
            }
        });
    }
}
