package com.example.myapplication.test.pr.facment.tctformat.activity;

import android.content.Context;
import android.os.Bundle;
import android.view.View;
import android.widget.ExpandableListView;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;

import com.example.myapplication.test.pr.facment.tctformat.R;
import com.example.myapplication.test.pr.facment.tctformat.adapter.CustomExpandableDataAdapter;
import com.example.myapplication.test.pr.facment.tctformat.contract.ExListContract;
import com.example.myapplication.test.pr.facment.tctformat.contract.ExListPresenter;
import com.example.myapplication.test.pr.facment.tctformat.data.preference.AppPreference;
import com.example.myapplication.test.pr.facment.tctformat.data.response.Data1;
import com.example.myapplication.test.pr.facment.tctformat.data.response.ExpKey;
import com.example.myapplication.test.pr.facment.tctformat.data.response.ExpSubData;
import com.example.myapplication.test.pr.facment.tctformat.databinding.ActivityExlistBinding;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

public class ExListActivity extends AppCompatActivity implements ExListContract.View {
    private ExListContract.Presenter presenter;
    private ExpandableListView expandableListView;
    private CustomExpandableDataAdapter adapter;

    private Context context;


    @Override
    public void onCreate(Bundle bundle) {
        super.onCreate(bundle);
        setContentView(R.layout.activity_exlist);

        this.context = this;
        AppPreference.install(context);

        expandableListView = findViewById(R.id.expandListView);
        expandableListView.setGroupIndicator(null);
//        expandableListView.setIndicatorBounds(600,200);
        if(android.os.Build.VERSION.SDK_INT < android.os.Build.VERSION_CODES.JELLY_BEAN_MR2) {
            expandableListView.setIndicatorBounds(expandableListView.getWidth()+ 80, 0);
        } else {
            expandableListView.setIndicatorBoundsRelative(expandableListView.getWidth()+ 80, 0);
        }


        expandableListView.setOnGroupExpandListener(new ExpandableListView.OnGroupExpandListener() {
            @Override
            public void onGroupExpand(int i) {
                Toast.makeText(context, "onGroupExpand : " + i, Toast.LENGTH_SHORT).show();
            }
        });

        expandableListView.setOnGroupCollapseListener(new ExpandableListView.OnGroupCollapseListener() {
            @Override
            public void onGroupCollapse(int i) {
                Toast.makeText(context, "onGroupCollapse : " + i, Toast.LENGTH_SHORT).show();
            }
        });

        expandableListView.setOnGroupClickListener(new ExpandableListView.OnGroupClickListener() {
            @Override
            public boolean onGroupClick(ExpandableListView expandableListView, View view, int i, long l) {
                Toast.makeText(context, "onGroupClick\n" + "그룹 위치 : " + i + ", 그룹 ID : " + l, Toast.LENGTH_SHORT).show();
                return false;
            }
        });

        expandableListView.setOnChildClickListener(new ExpandableListView.OnChildClickListener() {
            @Override
            public boolean onChildClick(ExpandableListView expandableListView, View view, int i, int i1, long l) {
                Toast.makeText(context, "onChildClick\n" + "그룹 위치 : " + i + ", Child 위치 : " + i1, Toast.LENGTH_SHORT).show();

                return false;
            }
        });
    }

    @Override
    public void onResume() {
        super.onResume();

        presenter = new ExListPresenter();
        presenter.setView(this);
        presenter.requestData();
    }

    private List<ExpKey> expKeyList;
    private HashMap<ExpKey, List<ExpSubData>> expKeyListHashMap;

    @Override
    public void setDatas(List<ExpKey> expKeyList, HashMap<ExpKey, List<ExpSubData>> expDetailDataList) {
        this.expKeyList = new ArrayList<>(expDetailDataList.keySet());
        this.expKeyListHashMap = expDetailDataList;
        adapter = new CustomExpandableDataAdapter(this, expKeyListHashMap, new CustomExpandableDataAdapter.OnHandleListener() {
            @Override
            public void clickBtn(int groupPos, int childPos) {

            }
        });
        expandableListView.setAdapter(adapter);
    }
}
