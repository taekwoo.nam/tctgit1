package com.example.myapplication.test.pr.facment.tctformat.util;

import android.content.Context;

public class Util {
    public static int getFileRId(Context context, String fileName) {
        String fileNameNoneExt = getFileNameExceptExt(fileName);
        int imgR = context.getResources().getIdentifier(fileNameNoneExt, "drawable", context.getPackageName());
        return imgR;
    }

    public static String getFileNameExceptExt(String fileName) {
        int pos = fileName .lastIndexOf(".");
        String _fileName = fileName.substring(0, pos);
        return _fileName;
    }

    public static String getExtNameExceptFileName(String fileName) {
        int pos = fileName.lastIndexOf( "." );
        String ext = fileName.substring( pos + 1 );
        return ext;
    }
}
