package com.example.myapplication.test.pr.facment.tctformat.data.preference;

import android.annotation.SuppressLint;
import android.app.Application;
import android.content.Context;
import android.content.SharedPreferences;

import androidx.annotation.NonNull;


public class AppPreference {
    private static final String TAG = "AppPreference";

    @SuppressLint("StaticFieldLeak")
    private static AppPreference INSTANCE;

    // PREFERENCE FILE NAME
    private static String PREF_FILE_NAME = "TCT_PREF_FILE";


    // PREFERENCE KEY
    private static final String PREF_TCT_SAVE_DATE = "PREF_TCT_SAVE_DATE";

    public static void install(Application application) {
        install(application.getApplicationContext());
    }

    public synchronized static void install(Context context) {
        if (INSTANCE == null) {
            INSTANCE = new AppPreference(context);
        }
    }

    public static AppPreference from(Context context) {
        install(context);

        return get();
    }

    public static AppPreference get() {
        return INSTANCE;
    }

    @NonNull
    private SharedPreferences mSharedPreferences;


    private AppPreference(Context context) {
        mSharedPreferences = context.getSharedPreferences(PREF_FILE_NAME, Context.MODE_PRIVATE);
    }

    public void setTCTData(String sort) {
        mSharedPreferences.edit().putString(PREF_TCT_SAVE_DATE, sort).apply();

    }

    public String getTCTData() {
        return mSharedPreferences.getString(PREF_TCT_SAVE_DATE, "");
    }

}
