package com.example.myapplication.test.pr.facment.tctformat.network.retrofit;

import com.example.myapplication.test.pr.facment.tctformat.data.response.Data1;
import com.example.myapplication.test.pr.facment.tctformat.data.response.Data2;
import com.example.myapplication.test.pr.facment.tctformat.util.Const;

import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.GET;
import retrofit2.http.Header;
import retrofit2.http.Headers;
import retrofit2.http.POST;
import retrofit2.http.Path;
import retrofit2.http.Query;

public interface DataGetService {


    @GET(Const.PATH1)
    Call<Data1> requestData1();

    @POST(Const.PATH2)
    Call<Data2> requestData2();


//    @GET(Const.PATH1)
//    Call<Data1> requestData1(
//            @Query("lineId") String lineId,
//                             @Query("pickupStopId")String pickupStopId,
//                             @Query("dropoffStopId")String dropoffStopId);
//
//    @POST(Const.PATH2)
//    Call<Data2> requestData2(@Path("path2") String path2,
//                             @Query("pickupStopId")String pickupStopId,
//                             @Query("dropoffStopId")String dropoffStopId);
}
