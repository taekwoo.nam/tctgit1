package com.example.myapplication.test.pr.facment.tctformat.adapter;

import android.content.Context;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseExpandableListAdapter;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.constraintlayout.widget.ConstraintLayout;
import androidx.recyclerview.widget.RecyclerView;

import com.example.myapplication.test.pr.facment.tctformat.R;
import com.example.myapplication.test.pr.facment.tctformat.data.response.Data1;
import com.example.myapplication.test.pr.facment.tctformat.data.response.ExpKey;
import com.example.myapplication.test.pr.facment.tctformat.data.response.ExpSubData;
import com.example.myapplication.test.pr.facment.tctformat.generated.callback.OnClickListener;
import com.example.myapplication.test.pr.facment.tctformat.util.Util;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

public class CustomExpandableDataAdapter extends BaseExpandableListAdapter {
    private Context ctx;
    private List<ExpKey> expandableKeyList;
    private HashMap<ExpKey, List<ExpSubData>> expandableDetailData;
    private OnHandleListener listener;

    public interface OnHandleListener {
        void clickBtn(int groupPos, int childPos);
    }

    public CustomExpandableDataAdapter(Context context, HashMap<ExpKey, List<ExpSubData>> expDetailData,
                                       OnHandleListener listener) {
        this.ctx = context;
        this.expandableKeyList = new ArrayList<>(expDetailData.keySet());
        this.expandableDetailData = expDetailData;
        this.listener = listener;
    }

    @Override
    public int getGroupCount() {
        return this.expandableKeyList.size();
    }

    @Override
    public int getChildrenCount(int i) {
        Log.e("taekwoo.nam", expandableDetailData.toString());

        return this.expandableDetailData.get(expandableKeyList.get(i)).size();
    }

    @Override
    public Object getGroup(int i) {
        return expandableKeyList.get(i);
    }

    @Override
    public Object getChild(int i, int i1) {
        return expandableDetailData.get(expandableKeyList.get(i)).get(i1);
    }

    @Override
    public long getGroupId(int i) {
        return i;
    }

    @Override
    public long getChildId(int i, int i1) {
        return i1;
    }

    @Override
    public boolean hasStableIds() {
        return false;
    }

    @Override
    public View getGroupView(int i, boolean b, View view, ViewGroup viewGroup) {
        if (view == null) {
            LayoutInflater inflater = (LayoutInflater) this.ctx.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            view = inflater.inflate(R.layout.expand_group_layout, null);
            ImageView img = view.findViewById(R.id.ivGroupIcon);
            img.setImageResource(Util.getFileRId(ctx, expandableKeyList.get(i).getImageFileName()));
        }
        return view;
    }

    @Override
    public View getChildView(int i, int i1, boolean b, View view, ViewGroup viewGroup) {
        if (view == null) {
            LayoutInflater inflater = (LayoutInflater) this.ctx.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            view = inflater.inflate(R.layout.expand_child_layout, null);
            TextView tvChild = view.findViewById(R.id.tvChildContent);
            tvChild.setText(expandableDetailData.get(expandableKeyList.get(i)).get(i1).getData1());
            Button btn = view.findViewById(R.id.btnChild);
            btn.setTag("Group : " + i + ", Child : " + i1);
            btn.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    Toast.makeText(ctx, (String)view.getTag(), Toast.LENGTH_SHORT).show();
                }
            });
        }


        return view;
    }

    @Override
    public boolean isChildSelectable(int i, int i1) {
        return true;
    }
}
