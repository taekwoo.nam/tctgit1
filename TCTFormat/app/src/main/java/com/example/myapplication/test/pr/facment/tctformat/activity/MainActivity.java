package com.example.myapplication.test.pr.facment.tctformat.activity;

import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.SpinnerAdapter;

import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.SearchView;
import androidx.databinding.DataBindingUtil;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.example.myapplication.test.pr.facment.tctformat.R;
import com.example.myapplication.test.pr.facment.tctformat.adapter.DataAdapter;
import com.example.myapplication.test.pr.facment.tctformat.data.response.Data1;
import com.example.myapplication.test.pr.facment.tctformat.databinding.ActivityMainBinding;
import com.example.myapplication.test.pr.facment.tctformat.contract.MainContract;
import com.example.myapplication.test.pr.facment.tctformat.contract.MainPresenter;

import java.util.ArrayList;
import java.util.List;

public class MainActivity extends AppCompatActivity implements MainContract.View{
    private ActivityMainBinding binding;
    private MainContract.Presenter presenter;
    private DataAdapter adapter;
    private RecyclerView recyclerView;
    private ArrayAdapter<String> spinnerAdapter;

    @Override
    public void onCreate(Bundle bundle) {
        super.onCreate(bundle);
        binding = DataBindingUtil.setContentView(this, R.layout.activity_main);
        presenter = new MainPresenter();
        presenter.setView(this);
        binding.setPresenter(presenter);
//        binding.recDataList.setLayoutManager(new LinearLayoutManager(this, RecyclerView.VERTICAL, false));
        binding.recDataList.setLayoutManager(new GridLayoutManager(this, 2));

        List<String> spinnerItems = new ArrayList<>();
        spinnerAdapter = new ArrayAdapter<>(this, android.R.layout.simple_spinner_item,spinnerItems);
        spinnerItems.add("t1");
        spinnerItems.add("t2");
        spinnerItems.add("t3");
        spinnerItems.add("t4");
        spinnerItems.add("t5");
        spinnerAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        binding.spList.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
                Log.e("taekwoo.nam", "spinner : " + i);
            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {

            }
        });
        binding.spList.setAdapter(spinnerAdapter);
        binding.searchView.setOnQueryTextListener(new SearchView.OnQueryTextListener() {
            @Override
            public boolean onQueryTextSubmit(String query) {
                Log.e("taekwoo.nam", "onQueryTextSubmit::" + query);
                adapter.filter(query);
                return false;
            }

            @Override
            public boolean onQueryTextChange(String newText) {
                Log.e("taekwoo.nam", "onQueryTextChange::" + newText);
//                adapter.filter(newText);
                return false;
            }
        });

    }

    @Override
    public void onResume() {
        super.onResume();

//        presenter.requestDates();
    }

    @Override
    public void setDatas(List<Data1> data1List) {
        Log.e("taekwoo.nam", "setDatas");
        if (adapter == null) {
            Log.e("taekwoo.nam", "setDatas::DataAdapter");
            adapter = new DataAdapter(this, data1List, new DataAdapter.OnHandlerListener() {
                @Override
                public void clickItem(int position) {
                    Log.e("taekwoo.nam" , "click item : " + position);
                }
            });

        }

        binding.recDataList.setAdapter(adapter);
        adapter.setDataList(data1List);
    }
}
