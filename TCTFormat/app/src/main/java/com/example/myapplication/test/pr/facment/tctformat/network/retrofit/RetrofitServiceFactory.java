package com.example.myapplication.test.pr.facment.tctformat.network.retrofit;

import com.example.myapplication.test.pr.facment.tctformat.network.OkHttpClientFactory;
import com.example.myapplication.test.pr.facment.tctformat.util.Const;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

import okhttp3.OkHttpClient;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public class RetrofitServiceFactory {

    private static final String TAG = "RetrofitServiceFactory";

    private static DataGetService dataGetService;


    private static Retrofit createRetrofit() {
        final OkHttpClient client = OkHttpClientFactory.api();
        final Gson gson = new GsonBuilder().setLenient().create();
        return new Retrofit.Builder()
                .client(client)
                .baseUrl(Const.HOST_URL)
                .addConverterFactory(GsonConverterFactory.create(gson))
                .build();
    }


    public static DataGetService dataGetService() {
        if (dataGetService == null) {
            dataGetService = createRetrofit().create(DataGetService.class);
        }
        return dataGetService;
    }
}
