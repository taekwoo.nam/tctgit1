package com.example.myapplication.test.pr.facment.tctformat.util;

public class Const {
    public static final String HOST_URL = "http://localhost:3001";
    public static final String PATH1 = "/v1/health";
    public static final String PATH2 = "/v1/health/{path1}";
}
