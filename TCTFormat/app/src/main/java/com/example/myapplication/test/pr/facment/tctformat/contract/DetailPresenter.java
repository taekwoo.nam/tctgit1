package com.example.myapplication.test.pr.facment.tctformat.contract;

import com.example.myapplication.test.pr.facment.tctformat.data.response.Data1;
import com.example.myapplication.test.pr.facment.tctformat.network.retrofit.RetrofitServiceFactory;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class DetailPresenter implements DetailContract.Presenter<DetailContract.View> {
    private DetailContract.View view;

    @Override
    public void setView(DetailContract.View view) {
        this.view = view;
    }

    public void requestData() {
        RetrofitServiceFactory.dataGetService().requestData1().enqueue(new Callback<Data1>() {
            @Override
            public void onResponse(Call<Data1> call, Response<Data1> response) {

            }

            @Override
            public void onFailure(Call<Data1> call, Throwable t) {

            }
        });
    }
}
