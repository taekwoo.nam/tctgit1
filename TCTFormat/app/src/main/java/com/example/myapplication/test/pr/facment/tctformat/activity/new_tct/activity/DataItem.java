package com.example.myapplication.test.pr.facment.tctformat.activity.new_tct.activity;

public class DataItem {

    public DataItem(String d1, String d2, String d3) {
        data1 = d1;
        data2 = d2;
        data3 = d3;
    }


    private String data1;
    private String data2;
    private String data3;

    public String getData1() {
        return data1;
    }

    public void setData1(String data1) {
        this.data1 = data1;
    }

    public String getData2() {
        return data2;
    }

    public void setData2(String data2) {
        this.data2 = data2;
    }

    public String getData3() {
        return data3;
    }

    public void setData3(String data3) {
        this.data3 = data3;
    }
}
