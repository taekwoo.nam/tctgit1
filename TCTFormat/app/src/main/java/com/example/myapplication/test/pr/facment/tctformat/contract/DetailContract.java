package com.example.myapplication.test.pr.facment.tctformat.contract;

public interface DetailContract {
    interface View {};

    interface Presenter<T extends View> {
        void setView(T view);
    }
}
