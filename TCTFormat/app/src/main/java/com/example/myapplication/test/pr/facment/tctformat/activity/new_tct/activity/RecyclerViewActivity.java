package com.example.myapplication.test.pr.facment.tctformat.activity.new_tct.activity;

import android.content.Intent;
import android.os.Bundle;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.example.myapplication.test.pr.facment.tctformat.R;

import java.util.ArrayList;

public class RecyclerViewActivity extends AppCompatActivity {
    private RecyclerView recyclerView;
    private DataAdapter adapter;
    private ArrayList<DataItem> dataItemArrayList = new ArrayList<>();

    @Override
    public void onCreate(Bundle bundle) {
        super.onCreate(bundle);
        setContentView(R.layout.activity_recycler);

        recyclerView = findViewById(R.id.recyclerView);
        recyclerView.setLayoutManager(new LinearLayoutManager(this));

        dataItemArrayList.add(new DataItem("d1", "d2", "d3"));
        dataItemArrayList.add(new DataItem("dd1", "dd2", "dd3"));
        dataItemArrayList.add(new DataItem("ddd1", "ddd2", "ddd3"));

        adapter = new DataAdapter(dataItemArrayList, new DataAdapter.OnItemClickListener() {
            @Override
            public void onItemClick(int position) {
                Intent intent = new Intent(RecyclerViewActivity.this, DetailActivity.class);
                startActivityForResult(intent,REQUEST_CODE);
            }
        });
        recyclerView.setAdapter(adapter);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == REQUEST_CODE) {

        }
    }

    private static int REQUEST_CODE = 111;
}
