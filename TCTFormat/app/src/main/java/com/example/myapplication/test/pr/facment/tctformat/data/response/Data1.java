package com.example.myapplication.test.pr.facment.tctformat.data.response;

import com.google.gson.annotations.SerializedName;

public class Data1 {
    @SerializedName("key")
    private String key;

    public String getKey() {
        return key;
    }

    public Data1(String key) {
        this.key = key;
    }

    @Override
    public String toString() {
        return "Data1{" +
                "key='" + key + '\'' +
                '}';
    }
}
