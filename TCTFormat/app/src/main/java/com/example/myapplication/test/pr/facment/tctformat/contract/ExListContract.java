package com.example.myapplication.test.pr.facment.tctformat.contract;

import com.example.myapplication.test.pr.facment.tctformat.data.response.Data1;
import com.example.myapplication.test.pr.facment.tctformat.data.response.ExpKey;
import com.example.myapplication.test.pr.facment.tctformat.data.response.ExpSubData;

import java.util.HashMap;
import java.util.List;

public interface ExListContract {
    interface View {
        void setDatas(List<ExpKey> expKeyList, HashMap<ExpKey, List<ExpSubData>> expDetailDataList);
    };

    interface Presenter<T extends View> {
        void setView(T view);
        void requestData();
    }
}
