package com.example.myapplication.test.pr.facment.tctformat.network;

import android.util.Log;

import java.util.concurrent.TimeUnit;

import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.Response;

public class OkHttpClientFactory {
    private static final String TAG = "OkHttpClientFactory";
    private static final long CONNECTION_TIMEOUT = 10000L;
    private static final long API_READ_TIMEOUT = 15000L; // API 수신
    private static final long API_WRITE_TIMEOUT = 15000L; // API 요청

    private static OkHttpClient api;

    public static OkHttpClient api() {
        if (api == null) {
            final OkHttpClient.Builder builder = newBuilder()
                    .connectTimeout(CONNECTION_TIMEOUT, TimeUnit.MILLISECONDS)
                    .readTimeout(API_READ_TIMEOUT, TimeUnit.MILLISECONDS)
                    .writeTimeout(API_WRITE_TIMEOUT, TimeUnit.MILLISECONDS);
            builder.addInterceptor(chain -> {
                final Request request = chain.request().newBuilder()
//                        .addHeader("UUID", AppPreference.get().getPrefRetrofitUuid())
                        .build();
                return chain.proceed(request);
            });
            api = build(builder);
        }
        return api;
    }

    public static OkHttpClient apiHeader() {
        if (api == null) {
            final OkHttpClient.Builder builder = newBuilder()
                    .connectTimeout(CONNECTION_TIMEOUT, TimeUnit.MILLISECONDS)
                    .readTimeout(API_READ_TIMEOUT, TimeUnit.MILLISECONDS)
                    .writeTimeout(API_WRITE_TIMEOUT, TimeUnit.MILLISECONDS);
            builder.addInterceptor(chain -> {
                final Request request = chain.request().newBuilder()
//                        .addHeader("UUID", AppPreference.get().getPrefRetrofitUuid())
                        .build();
                return chain.proceed(request);
            });
            api = build(builder);
        }
        return api;
    }


    public static OkHttpClient build(OkHttpClient.Builder builder) {

        return builder.build();
    }

    private static OkHttpClient.Builder newBuilder() {
        return new OkHttpClient.Builder();
    }
}
