package com.example.myapplication.test.pr.facment.tctformat.adapter;

import android.content.Context;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.constraintlayout.widget.ConstraintLayout;
import androidx.recyclerview.widget.RecyclerView;

import com.example.myapplication.test.pr.facment.tctformat.R;
import com.example.myapplication.test.pr.facment.tctformat.data.response.Data1;

import java.util.ArrayList;
import java.util.List;

public class DataAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {
    private List<Data1> dataList;
    private OnHandlerListener listener;
    private Context context;
    private List<Data1> filteredList;
    private boolean isFiltering = false;



    public DataAdapter(Context context, List<Data1> dataList, OnHandlerListener listener) {
        this.context = context;
        this.dataList = dataList;
        this.listener = listener;
    }

    @NonNull
    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        RecyclerView.ViewHolder viewHolder;
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.recycler_list_item,parent, false);
        viewHolder = new DataViewHolder(view, context, listener);
        return viewHolder;
    }

    @Override
    public void onBindViewHolder(@NonNull RecyclerView.ViewHolder holder, int position) {
        onDataBinding(holder, position);
    }

    @Override
    public int getItemCount() {
        if (isFiltering) {
            return filteredList.size();
        } else {
            return dataList.size();
        }
    }

    public class DataViewHolder extends RecyclerView.ViewHolder {
        private ConstraintLayout root;
        private ImageView ivIcon;
        private TextView tvContent;
        private Button btnStart;

        public DataViewHolder(@NonNull View itemView, Context context, OnHandlerListener listener) {
            super(itemView);

            root = itemView.findViewById(R.id.root);
            root.setOnClickListener(view -> {
                listener.clickItem(getAdapterPosition());
            });
            ivIcon = itemView.findViewById(R.id.ivIcon);
            tvContent = itemView.findViewById(R.id.tvContent);
            btnStart = itemView.findViewById(R.id.btnStart);
            btnStart.setOnClickListener(view -> {
                listener.clickItem(getAdapterPosition());
            });
        }
    }

    public interface OnHandlerListener {
        void clickItem(int position);
    }

    private void onDataBinding(RecyclerView.ViewHolder viewHolder, int position) {
        Log.e("taekwoo.nam", "onDataBinding");
        DataViewHolder holder = (DataViewHolder) viewHolder;
        Data1 data;
        if (!isFiltering) {
            data = dataList.get(position);
        } else {
            data = filteredList.get(position);
        }
        holder.tvContent.setText(data.getKey());
    }

    public void setDataList(List<Data1> dataList) {
        this.dataList = dataList;
        notifyDataSetChanged();
    }

    public void filter(String filter) {
        if (filter == null || "".equals(filter)) {
            isFiltering = false;
        } else {
            isFiltering = true;
        }

        if (filteredList == null) {
            filteredList = new ArrayList<>();
        }
        filteredList.clear();

        for (int i = 0; i < dataList.size(); i++) {
            if (dataList.get(i).getKey().contains(filter)) {
                filteredList.add(dataList.get(i));
            }
        }
        notifyDataSetChanged();
    }
}
