package com.example.myapplication.test.pr.facment.tctformat.data.response;

import com.google.gson.annotations.SerializedName;

public class ExpKey {
    @SerializedName("key")
    private String key;

    @SerializedName("type")
    private int type;

    @SerializedName("imageFileName")
    private String imageFileName;

    public ExpKey(String key, int type, String imageFileName) {
        this.key = key;
        this.type = type;
        this.imageFileName = imageFileName;
    }

    public String getKey() {
        return key;
    }

    public void setKey(String key) {
        this.key = key;
    }

    public int getType() {
        return type;
    }

    public void setType(int type) {
        this.type = type;
    }

    public String getImageFileName() {
        return imageFileName;
    }

    public void setImageFileName(String imageFileName) {
        this.imageFileName = imageFileName;
    }
}
