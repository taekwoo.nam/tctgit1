package com.example.myapplication.test.pr.facment.tctformat.activity.new_tct.activity;

import java.util.Comparator;

public class DataCompare {

    static class Data {
        String s1;
        int i1;
        String s2;

        public Data(String s1, int i1, String s2) {
            this.s1 = s1;
            this.i1 = i1;
            this.s2 = s2;
        }

        @Override
        public String toString() {
            return "Data{" +
                    "s1='" + s1 + '\'' +
                    ", i1=" + i1 +
                    ", s2='" + s2 + '\'' +
                    '}';
        }
    }

    static class PriorityComparator implements Comparator<Data> {
        @Override
        public int compare(Data data1, Data data2) {
            int result = data1.s1.compareTo(data2.s1);
            if (result != 0) {
                return result;
            }

            result = Integer.compare(data1.i1, data2.i1);
            if (result != 0) {
                return result;
            }

            return data1.s2.compareTo(data2.s2);
        }
    }
}
