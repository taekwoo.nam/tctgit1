package com.example.myapplication.test.pr.facment.tctformat.activity.new_tct.activity;

import androidx.room.Dao;
import androidx.room.Insert;
import androidx.room.Query;
import java.util.List;

@Dao
public interface NoteDao {

    @Insert
    void insert(Note note);

    @Query("SELECT * FROM note_table")
    List<Note> getAllNotes();
}