package com.example.myapplication.test.pr.facment.tctformat.contract;

import com.example.myapplication.test.pr.facment.tctformat.data.response.Data1;

import java.util.List;

public interface MainContract {
    interface View {
        void setDatas(List<Data1> data1List);
    };

    interface Presenter<T extends View> {
        void setView(T view);
        void requestDates();
    }
}
